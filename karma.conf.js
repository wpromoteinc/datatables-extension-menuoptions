module.exports = function (karma) {
    karma.set({

// base path, that will be used to resolve files and exclude
        basePath: './',

        frameworks: ['mocha'],

// list of files / patterns to load in the browser
        files: [
            {pattern: 'node_modules/chai/chai.js', include: true},
            'html/js/deps.js',
            'html/js/app.js',
            'html/js/datatables-*.js',
            'tests/**/*.js',

            'src/jade/_body_test.jade'
        ],


// list of files to exclude
        exclude: [
            'karma.conf.js'
        ],


// use dots reporter, as travis terminal does not support escaping sequences
// possible values: 'dots', 'progress', 'junit', 'teamcity'
// CLI --reporters progress
        reporters: ['progress', 'junit', 'coverage'],

        junitReporter: {
            // will be resolved to basePath (in the same way as files/exclude patterns)
            outputFile: 'junit-report/test-results.xml'
        },

        preprocessors: {
            'html/js/**/*.js': 'coverage',
            'src/jade/_body_test.jade': 'jade2js'
        },

//Code Coverage options. report type available:
//- html (default)
//- lcov (lcov and html)
//- lcovonly
//- text (standard output)
//- text-summary (standard output)
//- cobertura (xml format supported by Jenkins)
        coverageReporter: {
            dir: 'test/coverage',
            // Force the use of the Istanbul instrumenter to cover files
            instrumenter: {
                'js/*.js': ['istanbul']
            },
            reporters: [
                // reporters not supporting the `file` property
                { type: 'html', subdir: 'report-html' },
                { type: 'lcov', subdir: 'report-lcov' },
                // reporters supporting the `file` property, use `subdir` to directly
                // output them in the `dir` directory
                { type: 'lcovonly', subdir: '.', file: 'report-lcovonly.txt' }
            ]
        },


// web server port
        port: 9876,


// cli runner port
        runnerPort: 9100,


// enable / disable colors in the output (reporters and logs)
        colors: true,


// level of logging
// possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: karma.LOG_DEBUG,


// enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


// Start these browsers, currently available:
// - Chrome
// - ChromeCanary
// - Firefox
// - Opera
// - Safari (only Mac)
// - PhantomJS
// - IE (only Windows)
// CLI --browsers Chrome,Firefox,Safari
        browsers: ['Chrome'],


// If browser does not capture in given timeout [ms], kill it
        captureTimeout: 6000,


// Continuous Integration mode
// if true, it capture browsers, run tests and exit
        singleRun: true,

        plugins: [
            'karma-mocha',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-junit-reporter',
            'karma-coverage',
            'karma-jade2js-preprocessor'
        ],

        jade2JsPreprocessor: {
            locals: {
                tableData: [
                    ['id', 'first_name', 'last_name', 'email', 'country', 'ip_address', 'Currency', 'Amount'],
                    ['1', 'Alice', 'Jordan', 'ajordan0@people.com.cn',  'Ivory Coast', '228.196.196.69', 'Franc', '$9285.57'],
                    ['2', 'Kathy', 'Campbell', 'kcampbell1@skyrock.com',  'Chile', '134.127.15.8', 'Peso', '$15483.55'],
                    ['3', 'Stephanie', 'Butler', 'sbutler2@furl.net',   'United States', '51.253.202.209', 'Dollar', '$15205.30'],
                    ['4', 'Kethy', 'Hayes', 'khayes3@shinystat.com',   'Greece', '124.128.152.249', 'Euro', '$651.63'],
                    ['5', 'Virginia', 'Nelson', 'vnelson4@instagram.com',  'China', '143', '250.181.47  Yuan', 'Renminbi', '$14884.88'],
                    ['6', 'Nicole', 'Gutierrez', 'ngutierrez5@va.gov',  'Indonesia', '90.45.170.140', 'Rupiah', '$24435.33'],
                    ['7', 'Jacqueline', 'Mcdonald', 'jmcdonald6@taobao.com',   'Portugal', '14.120.128.9', 'Euro', '$12729.20'],
                    ['8', 'Irene', 'Duncan', 'iduncan7@alexa.com',  'Indonesia', '99.110.149.82', 'Rupiah', '$819.56'],
                    ['9', 'Helen', 'Garza', 'hgarza8@vistaprint.com',  'Canada', '69.133.27.25', 'Dollar', '$18093.73'],
                    ['10', 'Benjamin', 'Campbell', 'bcampbell9@constantcontact.com',  'Slovenia', '58.62.160.240', 'Euro', '$12570.55']
                ]
            }
        }

    });
}