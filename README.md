# The Datatables.net extended filter plugin

### Developer Notes ###
At first, we need the template in js variable, so it could be easely imported. For start it would be enough to have it as static html. We will prepare this all in two styles, default and bootstrap ones. With prepared core styles of course, so, if needed, more styles can be created.

1. The second, all kind of controlls in menu option should be in separate kinda module. With two groops, inside and outside form. Those that are in, should be processed only on submit. Others should be live. This sayd, all the stuff should be verry modular.
2. The third, we need some bound between our modules and datatables. I think it will be this: the orchestrator will init all modules, and the bridge will link this to proper events and processors in datatables plugin.

### About layout.###
1. The markup will be done in jade. Because it is simple.
2. The usage, imports and dependecies will be done in simple javascript.
3. And the core plugin will be compiled from typescript. This is the part that we should test.


```bash
npm install
# ensure that gulp and bower globally installed, if not:
npm install bower gulp tsd --global
bower install
tsd reinstall # install typings for TypeScript inner reference, jQuery for ex.
gulp
gulp webserver # runs local server, and opens tab in current browser window
```

To run test do the `thing`:

```bash
# be sure to make npm install first
./node_modules/.bin/karma start karma.conf.js 
```

## How to use the plugin?

Embed genereted file after `jqeury` and `jquery.datatables`.

```html
<script type="text/javascript" src="datatables-menuoption.js"/>
```

And in your scripts use this:

```javascript
$('#example').DataTable({
    menuOption: [{
      targets: 3
    }]
});
```