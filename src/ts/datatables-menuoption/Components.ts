/// <reference path="./components/Condition.ts" />
/// <reference path="./components/ValueToggle.ts" />
/// <reference path="./components/Search.ts" />
/// <reference path="./components/Sort.ts" />
/// <reference path="./components/ValueToggle.ts" />

/// <reference path="./Scope.ts" />

module DTExtention.MenuOption {

    export interface Component {

        obj: any;
        live: boolean;

        action(): void;
        reset(): void;

        inForm(form:any): void;

    }

    export class Components {

        wrapper: any;
        scope: any;
        form: any;

        // meh... duplicating reference and list. boring.
        list:string[] = [
            'Condition',
            'ValueToggle',
            'Search',
            'Sort'
        ];

        item = {};

        constructor(scope: DTExtention.MenuOption.Scope) {
            this.scope = scope;
            this.initForm(scope.wrapper.obj);
            this.list.map(this.initComponent);
        }

        initComponent = (name:string): void => {
            var component = DTExtention.MenuOption[name];
            this.item[name] = new component(this.scope);
            this.item[name].inForm(this.form);
        };

        actionComponent = (name:string): void => {
            if (!this.item[name].live) {
                this.item[name].action();
            }
        };

        resetComponent = (name:string): void => {
            if (!this.item[name].live) {
                this.item[name].reset();
            }
        };

        initForm = (wrapper:any): void => {
            this.form = wrapper.find('form');
            var _t = this;
            this.form.on('submit', function(e) {
                e.preventDefault();
                _t.list.map(_t.actionComponent);
                if (_t.form.find('.has-error').length === 0) {
                    _t.scope.wrapper.container.removeClass('reveal');
                    _t.scope.api.draw();
                }
            });
            this.form.on('reset', function(e) {
                e.preventDefault();
                _t.scope.wrapper.container.removeClass('reveal');
                _t.list.map(_t.resetComponent);
                _t.scope.api.draw();
            });
        };

    }
}