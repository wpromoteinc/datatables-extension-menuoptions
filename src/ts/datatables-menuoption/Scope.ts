module DTExtention.MenuOption {

    interface Requirements {
        wrapper: any;
        api: any;
        settings: any;
    }

    export class Scope {

        settings: any;
        wrapper: any;
        api: any;

        constructor(params: Requirements) {

            this.settings = params.settings;
            this.wrapper = params.wrapper;
            this.api = params.api;

        }

    }

}