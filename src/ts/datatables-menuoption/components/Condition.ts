/// <reference path="../Components.ts" />

/// <reference path="../Scope.ts" />

module DTExtention.MenuOption {

    interface Objects {
        toggler:any;
        condition: {
            value:any;
            fromTo:any;
        };
        input: {
            value:any;
            from:any;
            to:any;
        };
    }

    function neatIterator(obj:any, cb:Function) {
        for (var name in obj) {
            if (obj.hasOwnProperty(name)) {
                cb(name, obj[name]);
            }
        }
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    var Validate = {

        Date: function(dateString:string): boolean {
            // First check for the pattern
            if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString)) {
                return false;
            }

            // Parse the date parts to integers
            var parts = dateString.split('/');
            var day = parseInt(parts[1], 10);
            var month = parseInt(parts[0], 10);
            var year = parseInt(parts[2], 10);

            // Check the ranges of month and year
            if (year < 1000 || year > 3000 || month === 0 || month > 12) {
                return false;
            }

            var monthLength = [
                31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
            ];

            // Adjust for leap years
            if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
                monthLength[1] = 29;
            }

            // Check the range of the day
            return day > 0 && day <= monthLength[month - 1];
        }

    };

    export class Condition implements DTExtention.MenuOption.Component {

        selector = {
            toggler: 'select[data-dtf-condition-option]',
            condition: {
                value: '[data-dtf-condition="value"]',
                fromTo: '[data-dtf-condition="from-to"]'
            },
            input: {
                value: '[data-dtf-condition-value]',
                from: '[data-dtf-condition-from]',
                to: '[data-dtf-condition-to]'
            }
        };

        obj:Objects;
        live: boolean = true;
        scope:any;

        modeScope = {
            value: [
                'contains',
                'exclude',
                'starts_with',
                'ends_with',
                'exact',
                'date',
                'date_before',
                'date_after',
                'greater',
                'greater_or_equal',
                'less',
                'less_or_equal',
                'equal',
                'isnot'
            ],
            fromTo: [
                'between',
                'beyond'
            ]
        };

        currentMode = {
            mode: null,
            name: null
        };

        validate = {
            'date': Validate.Date,
            'date_before': Validate.Date,
            'date_after': Validate.Date
        };

        placeholder = {
            'date': 'mm/dd/yy',
            'date_before': 'mm/dd/yy',
            'date_after': 'mm/dd/yy'
        };

        constructor(scope: DTExtention.MenuOption.Scope) {
            this.scope = scope;
            this.obj = {
                toggler: scope.wrapper.obj.find(this.selector.toggler),
                condition: {
                    value: scope.wrapper.obj
                        .find(this.selector.condition.value),
                    fromTo: scope.wrapper.obj
                        .find(this.selector.condition.fromTo)
                },
                input: {
                    value: scope.wrapper.obj
                        .find(this.selector.input.value),
                    from: scope.wrapper.obj
                        .find(this.selector.input.from),
                    to: scope.wrapper.obj
                        .find(this.selector.input.to)
                }
            };
            this.initObjects();
        }

        initObjects = (): void => {
            this.obj.toggler.on('change', this.onChange);
        };

        onChange = (e: any): void => {
            var value = this.obj.toggler.val();
            var found = false;
            var __this = this;
            neatIterator(this.modeScope, function(mode, scopeList) {
                if (scopeList.indexOf(value) !== -1) {
                    __this.setMode(mode, value);
                    found = true;
                }
            });
            if (!found) {
                this.setMode(null, value);
            }
        };

        setMode(modeView:string, modeName:string): void {
            var __this = this;
            this.currentMode = {
                mode: modeView,
                name: modeName
            };
            if (this.placeholder.hasOwnProperty(modeName)) {
                this.validationValues()
                    .inputs.attr('placeholder', this.placeholder[modeName]);
            } else {
                this.validationValues()
                    .inputs.each(function() {
                        $(this)
                            .attr('placeholder', $(this).data('placeholder'));
                    });
            }
            neatIterator(this.obj.condition, function(name, object) {
               __this.obj.condition[name].hide();
               if (name === modeView) {
                   __this.obj.condition[name].show();
               }
            });
        }

        action(): void {
            console.log('sort value by condition', this.currentMode);

            if (this.findFilter() === -1) {
                $.fn.dataTable.ext.search.push(this.filterWrap());
            }

            if (!this.isValidInput()) {
                this.validationValues().inputs.addClass('has-error');
                this.reset();
            } else {
                this.validationValues().inputs.removeClass('has-error');
            }

            if (this.live) {
                this.scope.api.draw();
            }

        }

        reset(): void {
            console.log('reset condition');

            var index = this.findFilter();

            if (index !== -1) {
                $.fn.dataTable.ext.search.splice(index, 1);
            }

            if (this.live) {
                this.scope.api.draw();
            }

        }

        inForm(form: any): void {
            if (form.has(this.obj.toggler) !== 0 &&
                form.has(this.obj.condition.value) !== 0 &&
                form.has(this.obj.condition.fromTo) !== 0 ) {
                this.live = false;
            };
        }

        findFilter(): number {
            var index = -1;

            for (var i in $.fn.dataTable.ext.search) {
                if ($.fn.dataTable.ext.search.hasOwnProperty(i)) {
                    var item = $.fn.dataTable.ext.search[i];
                    if (item.menuOption && item.menuOption.obj === this) {
                        index = i;
                    }
                }
            }

            return index;
        }

        filterWrap(): Function {
            var _t = this;
            var filter = (settings:any, data:any, dIndex:any): boolean => {
                return _t.filterByCondition(settings, data, dIndex);
            };
            filter['menuOption'] = {
                component: 'Condition',
                obj: this
            };
            return filter;
        }

        filterByCondition(settings, data, dIndex): boolean {
            var runner = 'condition_' + this.currentMode.name;

            if (this.hasOwnProperty(runner)) {
                var currentVal = data[this.scope.settings.targets];
                return this[runner](settings, currentVal, dIndex);
            } else {
                return true;
            }
        }

        hasValidation(): boolean {
            return this.validate.hasOwnProperty(this.currentMode.name);
        }

        validationValues(): any {
            if (this.currentMode.mode === 'fromTo') {
                return {
                    inputs: this.obj.input.from.add(this.obj.input.to),
                    values: [this.obj.input.from.val(), this.obj.input.to.val()]
                };
            };
            if (this.currentMode.mode === 'value') {
                return {
                    inputs: this.obj.input.value,
                    values: [this.obj.input.value.val()]
                };
            };
            return {
                inputs: $(),
                values: []
            };
        }

        isValidInput(): boolean {
            if (this.hasValidation()) {
                var result = true;
                var values = this.validationValues().values;
                for (var i = values.length - 1; i >= 0; i--) {
                    var valid = this.validate[this.currentMode.name](values[i]);
                    if (!valid) {
                        result = false;
                    }
                }
                return result;
            } else {
                return true;
            }
        }

        // The condition list

        condition_empty = (settings, value, dIndex): boolean => {
            return !value;
        };
        condition_nonzero = (settings, value, dIndex): boolean => {
            return value;
        };
        condition_contains = (settings, value, dIndex): boolean => {
            return value.indexOf(this.obj.input.value.val()) !== -1;
        };
        condition_exclude = (settings, value, dIndex): boolean => {
            return value.indexOf(this.obj.input.value.val()) === -1;
        };
        condition_starts_with = (settings, value, dIndex): boolean => {
            return value.indexOf(this.obj.input.value.val()) === 0;
        };
        condition_ends_with = (settings, value, dIndex): boolean => {
            var suffix = this.obj.input.value.val();
            return value.indexOf(suffix, value.length - suffix.length) !== -1;
        };
        condition_exact = (settings, value, dIndex): boolean => {
            return value === this.obj.input.value.val();
        };
        condition_date = (settings, value, dIndex): boolean => {
            var userValue = new Date(this.obj.input.value.val());
            if (Validate.Date(value)) {
                var val = new Date(value);
                // compare miliseconds now
                return val.getTime() === userValue.getTime();
            } else {
                return false;
            }
        };
        condition_date_before = (settings, value, dIndex): boolean => {
            var userValue = new Date(this.obj.input.value.val());
            if (Validate.Date(value)) {
                var val = new Date(value);
                // compare miliseconds now
                return val.getTime() <= userValue.getTime();
            } else {
                return false;
            }
        };
        condition_date_after = (settings, value, dIndex): boolean => {
            var userValue = new Date(this.obj.input.value.val());
            if (Validate.Date(value)) {
                var val = new Date(value);
                // compare miliseconds now
                return val.getTime() >= userValue.getTime();
            } else {
                return false;
            }
        };
        condition_greater = (settings, value, dIndex): boolean => {
            var inputVal = this.obj.input.value.val();
            if (isNumeric(value) && isNumeric(inputVal)) {
                return parseFloat(inputVal) < parseFloat(value);
            }
            return inputVal < value;
        };
        condition_greater_or_equal = (settings, value, dIndex): boolean => {
            var inputVal = this.obj.input.value.val();
            if (isNumeric(value) && isNumeric(inputVal)) {
                return parseFloat(inputVal) <= parseFloat(value);
            }
            return inputVal <= value;
        };
        condition_less = (settings, value, dIndex): boolean => {
            var inputVal = this.obj.input.value.val();
            if (isNumeric(value) && isNumeric(inputVal)) {
                return parseFloat(inputVal) > parseFloat(value);
            }
            return inputVal > value;
        };
        condition_less_or_equal = (settings, value, dIndex): boolean => {
            var inputVal = this.obj.input.value.val();
            if (isNumeric(value) && isNumeric(inputVal)) {
                return parseFloat(inputVal) >= parseFloat(value);
            }
            return inputVal >= value;
        };
        condition_equal = (settings, value, dIndex): boolean => {
            return this.obj.input.value.val() === value;
        };
        condition_isno = (settings, value, dIndex): boolean => {
            return this.obj.input.value.val() !== value;
        };
        condition_between = (settings, value, dIndex): boolean => {
            var fromVal = this.obj.input.from.val();
            var toVal = this.obj.input.to.val();
            if (isNumeric(value) && isNumeric(fromVal) && isNumeric(toVal)) {
                return parseFloat(fromVal) <= parseFloat(value) &&
                       parseFloat(toVal) >= parseFloat(value);
            }
            return this.obj.input.from.val() <= value &&
                   this.obj.input.to.val() >= value;
        };
        condition_beyond = (settings, value, dIndex): boolean => {
            var fromVal = this.obj.input.from.val();
            var toVal = this.obj.input.to.val();
            if (isNumeric(value) && isNumeric(fromVal) && isNumeric(toVal)) {
                return parseFloat(fromVal) >= parseFloat(value) ||
                       parseFloat(toVal) <= parseFloat(value);
            }
            return this.obj.input.from.val() >= value ||
                   this.obj.input.to.val() <= value;
        };

    }
}