/// <reference path="../Components.ts" />

/// <reference path="../Scope.ts" />

module DTExtention.MenuOption {

    interface Objects {
        ascending:any;
        descending:any;
    }

    export class Sort implements DTExtention.MenuOption.Component {

        selector = {
            ascending: '[data-dtf-sort="asc"]',
            descending: '[data-dtf-sort="desc"]'
        };

        obj: Objects;
        scope: any;
        live: boolean = true;

        active = null;

        constructor(scope: DTExtention.MenuOption.Scope) {
            this.scope = scope;
            this.obj = {
                ascending: scope.wrapper.obj.find(this.selector.ascending),
                descending: scope.wrapper.obj.find(this.selector.descending)
            };
            this.initSorters();
        }

        initSorters = (): void => {
            this.obj.ascending.on('click', this.sortAsc);
            this.obj.descending.on('click', this.sortDesc);
        };

        sortAsc = (): void => {
            this.setActive(this.obj.ascending);
            console.log('this.live', this.live);
            if (this.live) {
                this.action();
            }
        };

        sortDesc = (): void => {
            this.setActive(this.obj.descending);
            if (this.live) {
                this.action();
            }
        };

        setActive(active:any): void {
            this.obj.ascending.removeClass('active');
            this.obj.descending.removeClass('active');
            if (active) {
                active.addClass('active');
            }
            this.active = active;
        }

        action(): void {
            if (this.active) {
                var direction = this.active.data('dtf-sort');
                this.scope.api
                    .order([this.scope.settings.targets, direction])
                    .draw();
            }
        }

        reset(): void {
            this.setActive(null);
        }

        inForm(form: any): void {
            if (form.has(this.obj.ascending).length !== 0 &&
                form.has(this.obj.descending).length !== 0) {
                this.live = false;
            };
        }

    }
}