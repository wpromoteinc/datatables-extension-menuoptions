/// <reference path="../Components.ts" />

/// <reference path="../Scope.ts" />

module DTExtention.MenuOption {

    interface Objects {
        input:any;
    }

    export class Search implements DTExtention.MenuOption.Component  {

        selector = {
            input: 'input[data-dtf-search]'
        };

        obj:Objects;
        live: boolean = true;
        scope: any;

        constructor(scope: DTExtention.MenuOption.Scope) {
            this.scope = scope;
            this.obj = {
                input: scope.wrapper.obj.find(this.selector.input)
            };
            this.initInput();
        }

        initInput = (): void => {
            this.obj.input.on('change keyup', this.onChange);
        };

        onChange = (): void => {
            if (this.live) {
                this.action();
            }
        };

        action(): void {
            this.scope.api
                      .column(this.scope.settings.targets)
                      .search(this.obj.input.val());

            if (this.live) {
                this.scope.api.draw();
            }
        }

        reset(): void {
            this.obj.input.val('');
            this.action();

            if (this.live) {
                this.scope.api.draw();
            }
        }

        inForm(form: any): void {
            if (form.has(this.obj.input).length !== 0) {
                this.live = false;
            };
        }

    }
}