/// <reference path="../Components.ts" />

/// <reference path="../Scope.ts" />

module DTExtention.MenuOption {

    interface Objects {
        selectAll:any;
        clear:any;
        values:any;
        valueContainer:any;
    }

    export class ValueToggle implements DTExtention.MenuOption.Component {

        selector = {
            selectAll: '[data-dtf-by-val-all]',
            clear: '[data-dtf-by-val-clear]',
            value: 'input[type="checkbox"]',
            valueContainer: '[data-dtf-by-val] .values'
        };

        obj:Objects;
        live: boolean = true;
        scope:any;

        listOfValues: string[] = [];

        constructor(scope: DTExtention.MenuOption.Scope) {
            this.scope = scope;
            this.obj = {
                selectAll: scope.wrapper.obj.find(this.selector.selectAll),
                clear: scope.wrapper.obj.find(this.selector.clear),
                values: scope.wrapper.obj.find(this.selector.value),
                valueContainer: scope.wrapper.obj
                    .find(this.selector.valueContainer)
            };
            this.initObjects();
        }

        initObjects = (): void => {

            this.obj.selectAll.on('click', this.selectAll);
            this.obj.clear.on('click', this.Clear);
            this.obj.valueContainer.delegate(this.obj.values, 'change keyup',
                    this.onChange);

            var valueTemplate = DTExtention.MenuOption
                                    .template.component.valueToggle;

            var columnData = this.scope.api
                .column(this.scope.settings.targets)
                .data()
                .unique();

            for (var i = columnData.length - 1; i >= 0; i--) {
                this.obj.valueContainer
                    .append(valueTemplate.replace(/\{value\}/g, columnData[i]));
            }

            this.obj.values = this.scope.wrapper.obj.find(this.selector.value);

        };

        onChange = (e:any): void => {
            var _t = this;
            this.listOfValues = [];
            this.obj.valueContainer
                .find('input').filter(':checked')
                .each(function() {
                    _t.listOfValues.push($(this).val());
                });
            if (this.live) {
                this.action();
            }
        };

        selectAll = (e:any): void => {
            e.preventDefault();
            this.obj.valueContainer
                .find(this.selector.value).prop('checked', true);
            this.onChange(e);
        };

        Clear = (e:any): void => {
            e.preventDefault();
            this.obj.valueContainer
                .find(this.selector.value).prop('checked', false);
            this.onChange(e);
        };

        findFilter(): number {
            var index = -1;

            for (var i in $.fn.dataTable.ext.search) {
                if ($.fn.dataTable.ext.search.hasOwnProperty(i)) {
                    var item = $.fn.dataTable.ext.search[i];
                    if (item.menuOption && item.menuOption.obj === this) {
                        index = i;
                    }
                }
            }

            return index;
        }

        reset(): void {

            var index = this.findFilter();

            if (index !== -1) {
                $.fn.dataTable.ext.search.splice(index, 1);
            }

            if (this.live) {
                this.scope.api.draw();
            }

        }

        action(): void {

            var index = this.findFilter();

            if (index === -1) {
                $.fn.dataTable.ext.search.push(this.filterWrap());
            }

            if (!this.obj.values.filter(':checked').length && index !== -1) {
                this.reset();
            }

            if (this.live) {
                this.scope.api.draw();
            }

        }

        filterWrap(): Function {
            var _t = this;
            var filter = (settings:any, data:any, dIndex:any): boolean => {
                if (!_t.listOfValues.length) {
                    return true;
                }
                var value = data[_t.scope.settings.targets];
                return _t.listOfValues.indexOf(value) !== -1;
            };
            filter['menuOption'] = {
                component: 'valueToggle',
                obj: this
            };
            return filter;
        }

        inForm(form: any): void {
            if (form.has(this.obj.selectAll).length !== 0 &&
                form.has(this.obj.clear).length !== 0 &&
                form.has(this.obj.valueContainer).length !== 0) {
                this.live = false;
            };
        }

    }
}
