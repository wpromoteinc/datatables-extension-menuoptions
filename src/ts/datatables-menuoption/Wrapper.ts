/// <reference path="../../../typings/jquery/jquery.d.ts" />

module DTExtention.MenuOption {
    export class Wrapper {

        obj: any;
        container: any;

        selector = {
            container: '[data-dtf-container]',
            obj: '[data-dtf-wrapper]'
        };

        constructor(DOMobj: any) {
            DOMobj.attr('data-dtf-here', '');
            this.obj = DOMobj.find(this.selector.obj);
            this.container = this.obj.find(this.selector.container);
            this.obj.on('click', function(e) {
                e.stopPropagation();
            });
            this.obj.find('input, button, select').on('keypress', function(e) {
                 if (e.which === 13) {
                    e.stopPropagation();
                 }
            });
            this.initToggler();
        }

        initToggler = (): void => {
            var __this = this;
            $(document).on('mouseup', function(e) {
                if (!__this.obj.is(e.target) &&
                    __this.obj.has(e.target).length === 0 ) {
                    __this.container.removeClass('reveal');
                }
            });
        };

    }
}