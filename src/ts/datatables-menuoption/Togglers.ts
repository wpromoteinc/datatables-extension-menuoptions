/// <reference path="../../../typings/jquery/jquery.d.ts" />

module DTExtention.MenuOption {

    var selector = {
        main: '[data-dtf-toggle]',
        target: '[data-dtf-toggle-target]'
    };


    class Item {

        obj: any;
        targetName: string;
        targets: any;

        constructor(DOMobj: any) {
            this.obj = DOMobj;
            this.targetName = this.obj.data('dtf-toggle');
            this.targets = $();

            this.obj.on('click', function(e) {
                e.stopPropagation();
            });
        }

        filterTargets = (wrapper: any): void => {
            var __this = this;
            wrapper.find(selector.target).each(function() {
                if ($(this).data('dtf-toggle-target') === __this.targetName) {
                    __this.targets = __this.targets.add($(this));
                }
            });
            this.obj.on('click', function() {
                __this.obj.toggleClass('reveal');
                __this.targets.toggleClass('reveal');
            });
        };
    }

    export class Togglers {

        items: any[] = [];

        constructor(wrapper: any) {
            var __this = this;
            wrapper.find(selector.main).each(function() {
                var item = new Item($(this));
                item.filterTargets(wrapper);
                __this.items.push(item);
            });
        }

    }
}