/// <reference path="./datatables-menuoption/Wrapper.ts" />
/// <reference path="./datatables-menuoption/Components.ts" />
/// <reference path="./datatables-menuoption/Togglers.ts" />
/// <reference path="./datatables-menuoption/Scope.ts" />

/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="../../typings/jquery.dataTables/jquery.dataTables.d.ts" />

module DTExtention.MenuOption {

    export var version:string = '0.0.1';

    export var template = {
        main: '{_template}',
        component: {
            valueToggle: '{_template-by-val}'
        }
    };

    export class MenuOption {

        wrapper:any;
        togglers:any;
        form:any;
        components:any;
        settings:any;
        scope: any;
        api: any;
        headerCell: any;

        constructor(settings: any, api: any) {

            this.settings = settings;
            this.api = api;

            var targets = this.settings.targets;

            if (!targets) {
                throw new Error('\'targets\' option missing');
            }

            if (!(Number(targets) === targets && targets % 1 === 0)) {
                throw new TypeError('\'targets\' must be integer');
            }

        }

        injectTemplate(tableHead: any): void {

            this.headerCell = tableHead
                .find('td, th').eq(this.settings.targets)
                .append(template.main);

            var m = DTExtention.MenuOption;

            this.wrapper = new m.Wrapper(this.headerCell);
            this.scope = new m.Scope({
                            wrapper: this.wrapper,
                            api: this.api,
                            settings: this.settings
                          });
            this.togglers = new m.Togglers(this.wrapper.obj);
            this.components = new m.Components(this.scope);

        }

    }

    export class Composite {

        list:MenuOption[] = [];

        constructor(settingsList: any, api: any) {

            for (var i in settingsList) {
                if (settingsList.hasOwnProperty(i)) {
                    var settings = settingsList[i];
                    // make this shorter...
                    var MenuOption = DTExtention.MenuOption.MenuOption;
                    var obj = new MenuOption(settings, api);
                    var dtSettings = api.settings()[0];
                    if (dtSettings.nTHead) {
                        obj.injectTemplate($(dtSettings.nTHead));
                    }
                    this.list.push(obj);
                }
            }

        }

    }

}

/*
 * DataTables API integration
 */

// Just to avoid TypeScript error
var DataTable = $.fn.dataTable;

DataTable.Api.register('menuOption()', function(settingsList, api) {
    console.log('DataTable.Api');
    return new DTExtention.MenuOption.Composite(settingsList, api);
});


/*
 * Initialisation
 */
$(document).on( 'init.dt.MenuOption', function (e, settings) {

    if ( e.namespace !== 'dt' ) {
        return;
    }

    var settingsList = settings.oInit.menuOption;
    var api = $(settings.nTable).DataTable();
    var MenuOption = new DTExtention.MenuOption.Composite(settingsList, api);

    $.fn.dataTable.MenuOption = MenuOption;
    $.fn.DataTable.MenuOption = MenuOption;

    return MenuOption;
});
