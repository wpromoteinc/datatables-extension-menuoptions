
module DTExtention.MenuOption {

    export var template = {
        main: '{_template-bootstrap}',
        component: {
            valueToggle: '{_template-by-val-bootstrap}'
        }
    };

}
