var fs = require('fs');

module.exports = {
  dist: {
    css: 'html/css/',
    js: 'html/js/',
    jsTests: 'tests/',
    img: 'html/images/',
    fonts: 'html/fonts/',
    html: 'html/'
  },
  src: {
    less: 'src/less/',
    img: 'src/img/',
    js: 'src/js/',
    typescript: 'src/ts/',
    fonts: 'src/fonts/',
    jade: 'src/jade/'
  },
  data: {
    site: {
      name: "DataTables.net extended filtering plugin"
    },
    csv: 'data/MOCK_DATA.csv'
  },
  var: {
    assets: {
      vendor: {
        js: {
          dataTables: {
            jquery: 'js/vendor/jquery.dataTables.min.js',
            bootstrap: 'js/vendor/dataTables.bootstrap.min.js'
          },
          jquery: 'js/vendor/jquery.min.js'
        },
        css: {
          bootstrap: 'css/vendor/bootstrap.min.css',
          dataTables: {
            jquery: 'css/vendor/jquery.dataTables.min.css',
            bootstrap: 'css/vendor/dataTables.bootstrap.min.css'
          }
        }
      },
      app: {
        css: "css/example.css",
        js: "js/example.js"
      },
      plugin: {
        css: {
          core: "css/datatables-menuoption-core.css",
          default: "css/datatables-menuoption-default.css",
          bootstrap: "css/datatables-menuoption-bootstrap.css"
        },
        js: "js/datatables-menuoption.js",
        template: {
          bootstrap: "js/datatables-menuoption-bootstrap.js"
        }
      }
    }
  }
};