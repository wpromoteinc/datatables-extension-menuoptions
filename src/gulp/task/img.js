var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    path = require('path'),
    globals = require('../globals');

gulp.task('img', function() { 

  var src = []
  src.push(globals.src.img + '**/*.*');
  src.push('bower_components/datatables/media/images/sort_*.*');

  return gulp.src(src)
    .pipe(plumber())
    .pipe(gulp.dest(globals.dist.img)); 

});
