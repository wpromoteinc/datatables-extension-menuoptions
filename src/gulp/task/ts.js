var gulp = require('gulp'),
    fs = require('fs'),
    path = require('path'),
    plumber = require('gulp-plumber'),
    tsc = require('gulp-typescript'),
    tslint = require('gulp-tslint')
    stylish = require('gulp-tslint-stylish'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    globals = require('../globals'),
    merge = require('merge-stream'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    foreach = require('gulp-foreach'),
    jade = require('jade');

/**
 * Lint all custom TypeScript files.
 */
gulp.task('ts-lint', function () {
    return gulp.src(globals.src.typescript + '**/*.ts')
      .pipe(plumber())
      .pipe(tslint({
        configuration: {}
      }))
      .pipe(tslint.report(stylish, {
        emitError: false,
        sort: true,
        bell: true
      }));
});

var collection = {
  'datatables-menuoption': [
    globals.src.typescript + 'datatables-menuoption.ts',
    globals.src.typescript + 'datatables-menuoption/**/*.ts',
    '!' + globals.src.typescript + 'tests/**/*.ts'
  ],
  'datatables-menuoption-bootstrap': [
    globals.src.typescript + 'datatables-menuoption-bootstrap.ts',
  ]
};

var tests = [
  globals.src.typescript + 'tests/**/*.ts'
];

var templates = {
  // filename: "it contentcs compiled from jade"
};

gulp.task('ts-templates', false, function() {

  return gulp.src([globals.src.jade + '**/_template*.jade'])
             .pipe(foreach(function(stream, file) {

                return stream.on('end', function() {

                  var templatePath = file.history[0];
                  var filename = path.basename(templatePath, '.jade');


                  var template = jade
                    .renderFile(templatePath)
                    .replace('\'', '\\\'');

                  templates[filename] = template;

                });

              }));

});

/**
 * Compile TypeScript and include references to library and app .d.ts files.
 */
gulp.task('ts-compile', ['ts-templates'], function () {

  var streams = [];

  // compile main typescript by each group
  Object.keys(collection).map(function(value, index) {

    var tsResult = gulp.src(collection[value])
                       .pipe(plumber())
                       .pipe(sourcemaps.init())
                       .pipe(tsc({
                          "module": "commonjs",
                          "compilerOptions": {
                            "target": "es6",
                            "sourceMap": true
                          }
                        }));

    tsResult.dts.pipe(gulp.dest(globals.dist.js));

    var p = tsResult.js;

    for (var filename in templates) {
      tsResult.js.pipe(replace('{'+filename+'}', templates[filename]));
    }

    tsResult.js.pipe(concat(value + '.js'))
               .pipe(sourcemaps.write('.'))
               .pipe(gulp.dest(globals.dist.js));

    streams.push(tsResult);

  });

  return merge.apply(merge, streams);

});


gulp.task("ts-uglify", ['ts-compile'], function() {

  var streams = [];

  Object.keys(collection).map(function(value, index) {

    var uglifiedPipe = gulp.src(globals.dist.js + value + '.js')
      .pipe(rename(function (path) {
        path.extname = ".min" + path.extname;
      }))
      .pipe(uglify())
      .pipe(gulp.dest(globals.dist.js))
      .on('end', function() {
        this.emit( 'unpipe' );  // remove the finished task from the stream
      });

    streams.push();

  });

  return merge.apply(merge, streams);

});

// gulp.task('ts', ['ts-lint', 'ts-uglify']);
gulp.task('ts', ['ts-lint', 'ts-compile']);

gulp.task("ts-tests", function() {

  // compile test files
  return gulp.src(tests)
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(tsc({
        "module": "commonjs",
        "compilerOptions": {
          "target": "es6",
          "sourceMap": true
        }
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(globals.dist.jsTests));

});
