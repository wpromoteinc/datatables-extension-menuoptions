var gulp = require('gulp'),
    globals = require('../globals');

gulp.task('vendor-css', function() {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/datatables/media/css/jquery.dataTables.min.css',
        'bower_components/datatables/media/css/dataTables.bootstrap.min.css'
    ]).pipe(gulp.dest(globals.dist.css + 'vendor/'));
});

gulp.task('vendor-js', function() {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/datatables/media/js/jquery.dataTables.min.js',
        'bower_components/datatables/media/js/dataTables.bootstrap.min.js'
    ]).pipe(gulp.dest(globals.dist.js + 'vendor/'));
});

gulp.task('vendor-fonts', function() {
    return gulp.src([
        'bower_components/bootstrap/fonts/**.*'
    ]).pipe(gulp.dest(globals.dist.css + 'fonts/'));
});

gulp.task('vendor', ['vendor-css', 'vendor-js', 'vendor-fonts']);
