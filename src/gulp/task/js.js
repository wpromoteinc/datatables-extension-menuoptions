var gulp = require('gulp'),
    globals = require('../globals'),
    path = require('path'),
    argv = require('yargs').argv,
    plumber = require('gulp-plumber'),
    // uglify = require('gulp-uglify'),
    through2 = require('through2'),
    notify = require('gulp-notify'),
    browserify = require('browserify');

gulp.task('js', function() {

  return gulp.src(globals.src.js + '*.js')
    .pipe(plumber())
    .pipe(through2.obj(function(file, enc, next) {

      browserify({
        detectGlobals: false,
        insertGlobals: false,
        debug: !argv.production,
        transform: ["debowerify"]
      })
        .add(file.path)
        .bundle(function(err, res) {
          // assumes file.contents is a Buffer
          file.contents = res;
          next(null, file);
        });

    }))
    .pipe(gulp.dest(globals.dist.js));
    // .pipe(notify({ message: '`js` task complete', onLast: true }));

});