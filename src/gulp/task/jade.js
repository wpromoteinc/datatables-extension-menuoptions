var gulp = require('gulp'),
    data = require('gulp-data'),
    plumber = require('gulp-plumber'),
    jade = require('jade'),
    gulpJade = require('gulp-jade'),
    notify = require('gulp-notify'),
    globals = require('../globals'),
    fs = require('fs'),
    csv2list = require('../util/csv2list');

gulp.task('jade', 'html preprocessor - jade, we do it NICE', function() { 

  var src = [];
  src.push(globals.src.jade + '**/*.jade');
  src.push('!' + globals.src.jade + '**/_*.jade');

  return gulp.src(src)
    .pipe(plumber())
    .pipe(data({
      site: globals.data.site,
      tableData: csv2list(',', '"', "\n")(fs.readFileSync(globals.data.csv, 'utf8')),
      assets: globals.var.assets,
      templateRender: function(file, options) {
        options.site = globals.data.site;
        options.assets = globals.var.assets;
        return jade.renderFile(file, options);
      }
    }))
    .pipe(gulpJade({
      pretty: true
    }))
    .pipe(gulp.dest(globals.dist.html));
    // .pipe(notify({ message: 'Markup task complete', onLast: true }));

});