var gulp = require('gulp'),
    watch = require('gulp-watch'),
    globals = require('../globals'),
    livereload = require('gulp-livereload');

var extend = function (target) {
  var sources = [].slice.call(arguments, 1);
  sources.forEach(function (source) {
    for (var prop in source) {
      target[prop] = source[prop];
    }
  });
  return target;
};

var watchIt = function(files, taskNames) {
  watch(files, extend({}, {
    verbose: true
  }), function(file, buffer) {
    gulp.start(taskNames);
  });
};

var taskDescription = 'watch changes, and trigger propriate tasks';

gulp.task('watch', taskDescription, ['default'], function() {

  watchIt(globals.src.img + '**/*.*', 'img');
  watchIt(globals.src.less + '**/*.less', 'less');
  watchIt(globals.src.js + '**/*.js', 'js');
  // watchIt(globals.src.fonts + '**/*.*', 'fonts');
  watchIt(globals.src.jade + '**/*.jade', 'jade');
  watchIt([
    globals.src.typescript + '**/*.ts',
    '!' + globals.src.typescript + 'tests/**/*.ts',
    globals.src.jade + '**/_template*.jade'
  ], 'ts');


  watchIt(globals.src.typescript + 'tests/**/*.ts', ['ts-tests']);

  livereload.listen({
    reloadPage: 'default.html'
  });

  watch(globals.dist.html + '**/*.*', {
    verbose: true
  }, function(file, buffer) {
    console.log('livereload');
    livereload.changed(file.path);
  });

});
