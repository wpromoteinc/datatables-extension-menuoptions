module.exports = function(delim, quote, lined) {
    delim = typeof delim == "string" ? new RegExp("[" + (delim || ",") + "]") : typeof delim == "undefined" ? "," : delim;
    quote = typeof quote == "string" ? new RegExp("^[" + (quote || '"') + "]") : typeof quote == "undefined" ? '"' : quote;
    lined = typeof lined == "string" ? new RegExp("[" + (lined || "\r\n") + "]+") : typeof lined == "undefined" ? "\r\n" : lined;

    function splitline(v) {
        // Split the line using the delimitor
        var arr = v.split(delim),
            out = [], q;
        for (var i = 0, l = arr.length; i < l; i++) {
            if (q = arr[i].match(quote)) {
                for (j = i; j < l; j++) {
                    if (arr[j].charAt(arr[j].length - 1) == q[0]) { break; }
                }
                var s = arr.slice(i, j + 1).join(delim);
                out.push(s.substr(1, s.length - 2));
                i = j;
            }
            else { out.push(arr[i]); }
        }

        return out;
    }

    function processLines(lines) {
        var newLines = []
        var keyList = lines[0];
        for (var i = 1, l = lines.length; i < l; i++) {
            if (lines[i].length != keyList.length) {continue;}
            var obj = {};
            for (var k in keyList) {
                obj[keyList[k]] = lines[i][k];
            }
            newLines.push(obj);
        }
        return newLines;
    }

    return function(text) {
        var lines = text.split(lined);
        for (var i = 0, l = lines.length; i < l; i++) {
            lines[i] = splitline(lines[i]);
        }
        var l = processLines(lines);
        return processLines(lines);
    };
}